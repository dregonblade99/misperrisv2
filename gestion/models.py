from django.db import models

# Create your models here.
class TipoVivienda(models.Model):
    nombre = models.CharField(max_length=50)
    
    def __str__(self):
        return self.nombre

class Region(models.Model):
    nombre = models.CharField(max_length=60)

    def __str__(self):
        return self.nombre

class Comuna(models.Model):
    nombre = models.CharField(max_length=60)
    region = models.ForeignKey(Region, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.nombre

class Persona(models.Model):
    nombre = models.CharField(max_length=50)
    rut = models.CharField(max_length=12)
    email = models.EmailField('Ingrese su Correo')
    telefono = models.IntegerField('Numero de Contacto')
    fecha = models.DateField('Año de Nacimiento')
    contraseña = models.CharField(max_length=50)
    """
    tipVivi = models.ForeignKey(TipoVivienda, on_delete=models.SET_NULL, null=True)
    nomRegion = models.ForeignKey(Region, on_delete=models.SET_NULL, null=True)
    nomComuna = models.ForeignKey(Comuna, on_delete=models.SET_NULL, null=True)
"""
    def __str__(self):
        return '{} {} {} {} {} {}'.format(self.nombre,str(self.email),self.rut,str(self.telefono),str(self.fecha),self.contraseña)
    

class Mascota(models.Model):
    estado_masc =(
        ('Disponible','Disponible'),
        ('Rescatado', 'Rescatado'),
        ('Adoptado','Adoptado')
    )

    nombre = models.CharField(max_length=40)
    foto = models.ImageField(upload_to="fotos_pets")
    raza = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=100)
    estado = models.CharField(max_length=20, choices=estado_masc,default = 'Rescatado')

    def __str__(self):
        return self.nombre
    




    


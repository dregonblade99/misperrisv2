from django.shortcuts import render,render_to_response
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from .models import Persona, Mascota
from .form import PersonaForm, MascotaForm
# Create your views here.

def index(request):
    return render(request, 'index.html')

def registro(request):

    context={
    }

    if request.method=='POST':
        estado=False   
        cont= Persona()
        cont.nombre=request.POST['nombre']
        cont.rut=request.POST['rut']
        cont.email=request.POST['email']
        cont.telefono=request.POST['telefono']
        cont.fecha=request.POST['fecha']
        cont.contraseña=request.POST['clave']
        cont.save()
        estado=True
        context={    
                "estado":estado
            }
        return render(request,'registro.html',context)
    return render(request,'registro.html',context)

def mascotaNnueva(request):
    if request.method=="POST":
        form = MascotaForm(request.POST , request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/Mascota/listar/')
    else:
        form = MascotaForm()

    return render(request,'agregar_masc.html',{'form':form})
from django import forms
from .models import Persona, Mascota, TipoVivienda

class TipoViviendaForm(forms.Form):
    class Meta:
        model=TipoVivienda
        fields="__all__"

class PersonaForm(forms.Form):
    class Meta:
        model=Persona
        fields="__all__"#parametros que queremos que aparescan

class MascotaForm(forms.ModelForm):
    class Meta:
        model= Mascota
        fields = "__all__"


